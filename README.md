# Too_Code_Union_test

Making test task for Too Code Union company


## Description
Building an easily extensible application from scratch.
Website aggregator for arch. developers

Configuring webpack 
Webpack-dev-server with env variables  
Typescript  
Link React  
Configuring css in webpack  
Css modules  
Adding routing and code splitting  
Styles organization  
Making own className plugin  
Restructuring and organization according to FSD principles  


## Start project
Choose build config for development or production
```
git clone
npm install
npm run build:dev or npm run build:prod
```

